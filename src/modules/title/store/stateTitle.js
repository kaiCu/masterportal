/**
 * User type definition
 * @typedef {object} TitleState
 * @property {string} title defines the default title of the portal.
 * @property {string} logo defines the default logo to display.
 * @property {string} link defines the default link which is called via click on the title.
 * @property {string} toolTip defines the default toolTip. String which is displayed if you hover over the title.
 */
export default {
    title: undefined,
    logo: undefined,
    link: undefined,
    toolTip: undefined
};
