/**
 * @typedef {number} MapMode
 **/

/**
 * Enum for map mode.
 * @readonly
 * @enum {number}
 */
export const MapMode = {
    "MODE_2D": 0,
    "MODE_3D": 1,
    "MODE_OB": 2
};
